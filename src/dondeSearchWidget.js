import hash from 'object-hash';

const savedStates = {};

// meant to be used by react's useEffect.
const reactEffect = (rootDivId, setResults, dondeOptions) => () => {
  let sdk;
  const dondeRootId = 'donde_root_inner';

  document.getElementById(rootDivId).innerHTML = `<div id="${dondeRootId}"></div>`;

  window.DondeWeb.initVisualSearch({
    api_key: window.api_key,
    app_id:  window.app_id,
    float_on_scroll: true,
    float_margin: 0,
    ...dondeOptions,
    selector: '#' + dondeRootId,
    animate_on_load: false,
  }, function(_sdk) {
    sdk = _sdk;
    // load previous state whenever widget loads with the same options again. 
    const savedState = savedStates[hash(dondeOptions)];
    if (savedState) {
      sdk.setState(savedState);
    }

    const peformSearch = () => {
      sdk.performSearch({})
        .then(function(results) {
          // display results
          setResults(results.results)
        })
        .catch(function(err) {
           console.error("An error has occured", err);
        })
    };

    sdk.subscribeToSearchChanged(peformSearch); // run on every click on the widget to fetch new results
    peformSearch(); // run on widget init
  })

  // This is the optional cleanup mechanism for effects. Every effect may return a function that cleans up after it. This lets us keep the logic for adding and removing subscriptions close to each other. They’re part of the same effect!
  return function cleanup() {
    savedStates[hash(dondeOptions)] = sdk.getState(); // save state for future loads with same options
    setResults([]);
  };
}

export default reactEffect
