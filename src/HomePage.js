import React from 'react';

const HomePage = () => {
  return (
    <div className="App-homePage">
      <h1>Welcome to Donde!</h1>
      <p>This is a demo one-page app that demonstrates how to efficiently incorportate the Donde Visual Search Widget in <b>React apps</b></p>
    </div>
  );
};

export default HomePage;
