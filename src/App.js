import React from 'react';
import {useRoutes, navigate} from 'hookrouter';
import HomePage from './HomePage';
import Category from './Category';
import './App.css';

const CATEGORIES = ['Dress', 'Top', 'Jacket', 'Sweater', 'Shoe'];

const routes = {
  '/': () => <HomePage />,
  '/products/:category': ({ category }) => <Category category={category} />
};

const App = () => {
  const routeResult = useRoutes(routes);

  if (!window.api_key) {
    alert('You must set api_key and app_id in index.html')
    return null;
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src='/logo.png' className="App-logo" alt="logo" />        
      </header>
      <nav className="App-nav">
        <NavLink href="/">Home</NavLink>
        { CATEGORIES.map( category => 
          <NavLink
            href={`/products/${category}`}
            key={category}
          >
            { category }
          </NavLink>
        )}
      </nav>
      <main className="App-main">
        { routeResult }
      </main>
    </div>
  );
}

const NavLink = ({ href, children }) => <a onClick={ () => navigate(href)}>{ children }</a>;

export default App;
