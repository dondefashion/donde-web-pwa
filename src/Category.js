import React, { Fragment, useState, useEffect } from 'react';
import hash from 'object-hash';
import dondeSearchWidgetEffect from './dondeSearchWidget';

const Category = ({ category }) => {
  const [results, setResults] = useState([]);
  const dondeOptions = { lock_category: category };
  const divId = `search_widget_container_${category}`; // better be a different ID on every widget init to make sure react re-renders the div

  useEffect(
    dondeSearchWidgetEffect(divId, setResults, dondeOptions),
    [hash(dondeOptions)] // important! re-run this effect only when `dondeOptions` actually change. otherwise it will run on every re-render.
  )

  return <Fragment>
    <h1>{ category }</h1>
    <div id={divId}></div>
    <Results results={results} />
  </Fragment>
};

const Results = ({ results }) => {
  return <div className="App-results">
    { results
      .map(r => r.main_images.viber_orig.url)
      .map( (src, index) => <img src={src} key={index} alt={index} />)
    }
  </div> 
};

export default Category;
