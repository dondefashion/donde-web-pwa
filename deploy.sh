#!/bin/bash
CLIENT_BUCKET=react-demo.search.donde-app.com

npm run build

gsutil -m rm gs://$CLIENT_BUCKET/**
gsutil -m rsync -R ./build "gs://$CLIENT_BUCKET"
gsutil -m acl set -R -a public-read "gs://$CLIENT_BUCKET"
gsutil -m setmeta -r -h "Cache-Control:public, max-age=5, must-revalidate, no-cache" $CLIENT_BUCKET
gsutil web set -m index.html -e index.html "gs://$CLIENT_BUCKET"
