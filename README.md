This is a demo one-page app that demonstrates how to efficiently incorportate the Donde Visual Search Widget in React apps.

In `index.html` replace the `api_key` and `app_id` with the correct values. You may contact Donde for them.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

